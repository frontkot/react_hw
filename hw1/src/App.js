import React from 'react';
import './App.css';
import Button from './components/Button/Button';
import './components/Button-block.scss'; 
import Modal from './components/Modal/Modal';

class App extends React.Component 
{

  state = {
    isOpen: false,
    headerContent: '',
    textContent: false
  }

  openModal(header, text) {
    this.setState({ isOpen: true, headerContent: header, textContent: text});
  }

  handleSubmit = () => {
    alert('You did it!');
    this.setState({ isOpen: false });
  }

  handleCancel = () => {
    this.setState({ isOpen: false });
  }
     

  render() {
    return (
      <div className="App">
        <div className='button-block'>
          <Button 
            text='Open first modal' 
            backgroundColor='maroon' 
            onClick={() => this.openModal('First modal window', true)} 
            className='my-button'/>
          <Button 
            text='Open second modal' 
            backgroundColor='blue' 
            onClick={() => this.openModal('Second modal window', false)} 
            className='my-button'/>
        </div>
          <Modal 
            header={this.state.headerContent} 
            onClick={this.openModal}
            closeButton={true}
            text={this.state.textContent} 
            isOpen={this.state.isOpen}
            onCancel={this.handleCancel}
            onSubmit={this.handleSubmit}
            />
      </div>
    )
}

}

export default App;

