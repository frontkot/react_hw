import React, { Component } from 'react';
import './Button.scss';
import PropTypes from 'prop-types';
import '../Modal/Modal.scss';




class Button extends Component {  
    static propTypes = {
        onClick: PropTypes.func,
        text: PropTypes.string,
        color: PropTypes.string,
    };
    
    static defaultProps = {
        onClick: () => {},
        text: '',
        color: ''
    };


    render() {
        const {className, backgroundColor, onClick, text} = this.props; 

        return (
            <button className={className} style={{ backgroundColor: backgroundColor }}  onClick={onClick} >
                    {text}
            </button>
            
        );
    }
}

export default Button;