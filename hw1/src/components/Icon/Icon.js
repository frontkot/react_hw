import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Icon.scss';

class Icon extends Component {
    static propTypes = {
        onClick: PropTypes.func,
        className: PropTypes.string
    };
    
    static defaultProps = {
        onClick: () => {},
        className: ''
    };


    render() {
        let {className, onClick} = this.props;

        return (
            <i 
                className={className} 
                onClick={onClick}
            />
                
        );
    }
}

export default Icon;