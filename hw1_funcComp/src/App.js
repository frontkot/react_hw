import React, {useState} from 'react';
import './App.css';
import Button from './components/Button/Button';
import './components/Button-block.scss'; 
import Modal from './components/Modal/Modal';

const App = () => {

  const [isOpen, setIsOpen] = useState(false);
  const [headerContent, setHeaderContent] = useState('');
  const [textContent, setTextContent] = useState(false);

  const openModal = (header, text) => {
    setIsOpen(true);
    setHeaderContent(header);
    setTextContent(text)
  }

  const handleSubmit = () => {
    alert('You did it!');
    setIsOpen(false);
  }

  const handleCancel = () => {
    setIsOpen(false);
  }
     

  return (
    <div className="App">
      <div className='button-block'>
        <Button 
          text='Open first modal' 
          backgroundColor='maroon' 
          onClick={() => openModal('First modal window', true)} 
          className='my-button'
        />
        <Button 
          text='Open second modal' 
          backgroundColor='blue' 
          onClick={() => openModal('Second modal window', false)} 
          className='my-button'
        />
      </div>
        <Modal 
          header={headerContent} 
          onClick={openModal}
          closeButton={true}
          text={textContent} 
          isOpen={isOpen}
          onCancel={handleCancel}
          onSubmit={handleSubmit}
          />
    </div>
  )
}

export default App;

