import React from 'react';
import PropTypes from 'prop-types';

import './Button.scss';

function Button({
    className, backgroundColor, onClick, text
}) {  

        return (
            <button className={className} style={{ backgroundColor: backgroundColor }}  onClick={onClick} >
                    {text}
            </button>
            
        );
}

export default Button;

Button.propTypes = {
    onClick: PropTypes.func,
    text: PropTypes.string,
    color: PropTypes.string,
};

Button.defaultProps = {
    onClick: () => {},
    text: '',
    color: ''
};


