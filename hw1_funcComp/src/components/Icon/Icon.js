import React from 'react';
import PropTypes from 'prop-types';

import './Icon.scss';

function Icon(props) {
    const {className, onClick} = props;

    return (
        <i 
            className={className} 
            onClick={onClick}
        />
            
    );
}

export default Icon;

Icon.propTypes = {
    onClick: PropTypes.func,
    className: PropTypes.string
};

Icon.defaultProps = {
    onClick: () => {},
    className: ''
};
