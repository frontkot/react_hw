import React from 'react';
import './Modal.scss';
import Button from '../Button/Button';
import PropTypes from 'prop-types';
import Icon from '../Icon/Icon';

function Modal(props) {

    const handleClick = (e, className) => {
        e.preventDefault()
        if(e.target.className === className){
            return props.onCancel()
        }
    }
    

    const {header, onSubmit, onCancel, text, isOpen, closeButton} = props;

    return (
        <div className='modal'>
            {isOpen &&
                <div className='modal-window' 
                    onClick={(e) => handleClick(e, 'modal-window')}
                >
                    <div className={text ? 'modal-block' : 'modal-container'}>
                        <div className={text ? 'modal-header' : 'modal-head'}>
                            <h3>{header}</h3>
                            {closeButton && <Icon onClick={onCancel} className='modal-icon'/>}
                        </div>
                        <div className='modal-content'>
                            <p className='modal-text'>
                                {text ? firstModalText : secondModalText}
                            </p>
                            <div className='modal-footer'>
                                <Button className={text ? 'modal-button' : 'modal-clicker'} 
                                        onClick={onSubmit} 
                                        backgroundColor={text ? '#b3382c': '#cab70a'} 
                                        text='Ok' 
                                />
                                <Button className={text ? 'modal-button' : 'modal-clicker'} 
                                        onClick={onCancel} 
                                        backgroundColor={text ? '#b3382c': '#cab70a'} 
                                        text='Cancel' 
                                />
                            </div>
                        </div>
                    </div>
                </div> 
            }
        </div>
    );
}

export default Modal;



const firstModalText = '“Hi Amy! Your mum sent me a text. You forgot your inhaler. Why don’t you turn your phone on?” Amy didn’t like technology. She never sent text messages and she hated Facebook too.';
const secondModalText = 'Amy normally hated Monday mornings, but this year was different. Kamal was in her art class and she liked Kamal. She was waiting outside the classroom when her friend Tara arrived.';

Modal.propTypes = {
    isOpen: PropTypes.bool,
    text: PropTypes.bool,
    header: PropTypes.string,
    onCancel: PropTypes.func,
    onSubmit: PropTypes.func
};

Modal.defaultProps = {
    isOpen: false,
    text: false,
    header: '',
    onCancel: () => {},
    onSubmit: () => {}
};