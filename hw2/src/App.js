import React, { useState, useEffect } from 'react';
import Header from './components/Header/Header'
import Content from './components/Content/Content';
import Footer from './components/Footer/Footer';
import Modal from './components/Modal/Modal'
import axios from 'axios';

import './Container.scss';
import './App.css';

const checkFav = () => {
  let arr = JSON.parse(localStorage.getItem('favorites-items'))

  if (arr !== null) {
      return arr
  } else {
    return []
  }
}

const checkCart = () => {
  let arr = JSON.parse(localStorage.getItem('cart-items'))

  if (arr !== null) {
      return arr
  } else {
    return []
  }
}


function App() {

  const [isOpen, setIsOpen] = useState(true);
  const [content, setContent] = useState([]);
  const [favList, setFavList] = useState(checkFav());
  const [openModal, setOpenModal] = useState(false);
  const [cartList, setCartList] = useState(checkCart());
  const [item, setItem] = useState('')

  
  const getContent = () => {
    return axios('./items.json')
    .then(res => setContent(res.data.items))
  }
  useEffect(() => {
    getContent()
  }, [])

 
  const changeFavorites = (id, key, item) => {
    let arr = item;
    if (arr.includes(id)) {
       arr = arr.filter(i => i !== id)
    } else {
      arr.push(id);
    }
    localStorage.setItem(key, JSON.stringify(arr))

    if (key === 'favorites-items') {
      setFavList(JSON.parse(localStorage.getItem('favorites-items')))
    } else if (key === 'cart-items') {
      setCartList(arr)
    }

 
  }

  const addToCart = () => {

    if(JSON.parse(localStorage.getItem('cart-items')) !== null) {
      if (JSON.parse(localStorage.getItem('cart-items')).includes(item)) {
        alert('This product already added to cart')
      } else {
        alert('You add this product to cart');
        changeFavorites(item, 'cart-items', cartList)
      }
    } else {
      alert('You add this product to cart');
      changeFavorites(item, 'cart-items', cartList)
    }
    setOpenModal(false);
  } 


  return (
    <div className='App'>
      <Header 
        className='main-header' 
        showContent={() => setIsOpen(!isOpen)}
        isOpen={isOpen}
      />
      <Content 
        className='main-content'
        isOpen={isOpen}
        content={content}
        changeBg={(e) => changeFavorites(e.target.id, 'favorites-items', favList)}
        favList={favList}
        openModal={(e) => {
          setOpenModal(!openModal)
          setItem(e.target.id)
        }}
      />
      <Modal 
          header={'Choose this product'} 
          onClick={() => setOpenModal(!openModal)}
          text={'The item will be added to the cart'} 
          isOpen={openModal}
          onSubmit={(e) => addToCart(e)}
        />
      <Footer 
        className='footer-bg'
        classWithoutContent='footer-bg footer-fixed'
        text='Aaa...Together because...'
        isOpen={isOpen}
      />     
    </div>
  );
}

export default App;
