import React from 'react';

import './Footer.scss'

const Footer = ({
    className, text, isOpen, classWithoutContent
}) => {


    return (
        <div className={isOpen ? className : classWithoutContent}>
            <div className='container'>
                <p>{text}</p>
            </div>
        </div>
    );
};

export default Footer;