import React from 'react';
import Button from '../Button/Button';

import './Header.scss'

const Header = ({
    className, showContent, isOpen
}) => {

    return (
        <div className='header-bg'>
            <div className='container'>
                <div className={className}>
                    <h3>This is header</h3>
                    <p>Here can be your advertising</p>
                    <Button 
                        className='header-button' 
                        onClick={showContent}
                    >
                        {isOpen ? 'Hide content'  : 'Show content'}
                    </Button>
                </div>
            </div>
        </div>
    );
};

export default Header;

Header.defaultProps = {
    className: 'header',
    showContent: false
}