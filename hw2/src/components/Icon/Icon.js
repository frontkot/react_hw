import React from 'react';
import PropTypes from 'prop-types';

import './Icon.scss';

const Icon = ({
  className, changeBg, article, name, favItem, onClick
}) => {

  return (
    <i
      item={name}
      className={className}
      onClick={onClick ? onClick : changeBg}
      style={{backgroundColor: favItem ? 'yellow' : '#E9E9E9'}} 
      id={article}
    />
  );
};

export default Icon;

Icon.propTypes = {
  className: PropTypes.string,
  changeBg: PropTypes.func,
  name: PropTypes.string,
  favItem: PropTypes.bool,
  article: PropTypes.string
};

Icon.defaultProps = {
  className: '',
  name: '',
  article: '',
  favItem: [],
  changeBg: () => {}
};


