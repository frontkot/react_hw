import React from 'react';
import Icon from '../Icon/Icon';
import Button from '../Button/Button';
import PropTypes from 'prop-types';


import './Item.scss'

const Item = ({
    changeBg, className, name, price, url, article, color, favItem, openModal
}) => {

    return (
        <li id={article} className={className}>
            <h3>{name}</h3>
            <Icon 
                name={name}
                changeBg={changeBg} 
                className='item-favorites'         
                article={article}
                favItem={favItem}
            />
            <img 
                src={url} 
                style={{borderRadius: '10px'}} 
                alt='item-img'
            />
            <p>Price - {price}</p>
            <p>Article: {article}</p>
            <p>Color: {color}</p>
            <Button 
                className='item-button' 
                onClick={openModal}
                id={article}
            >
                Add to Card
            </Button>
        </li>
    );
};

export default Item;


Item.propTypes = {
    className: PropTypes.string, 
    name: PropTypes.string,
    price: PropTypes.string,
    url: PropTypes.string ,
    article: PropTypes.string,
    color: PropTypes.string,
    favItem: PropTypes.bool,
    changeBg: PropTypes.func,
    openModal: PropTypes.func
};

Item.defaultProps = {
    className: '', 
    name: '',
    price: '',
    url: '',
    article: '',
    color: '',
    favItem: false,
    changeBg: () => {},
    openModal: () => {}
};