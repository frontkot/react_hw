import React from 'react';
import Item from '../Item/Item'
import PropTypes from 'prop-types';

import './ListItem.scss'

const ListItem = ({
    items, className, changeBg, favList, openModal
}) => {

    const productItems =  ( 
        items.map((e) => 
            <Item 
                className='product-item' 
                changeBg={changeBg}
                name={e.name}
                price={e.price}
                url={e.url}
                article={e.article}
                color={e.color}
                key={e.article}
                favItem={favList.includes(e.article) ? true : false}
                openModal={openModal}
            />
    )
)
    
    return (
        <ul className={className}>
            {productItems}
        </ul>
    );
};

export default ListItem;


ListItem.propTypes = {
    isOpen: PropTypes.bool,
    className: PropTypes.string,
    items: PropTypes.array,
    changeBg: PropTypes.func,
    favList: PropTypes.array,
    openModal: PropTypes.func
};

ListItem.defaultProps = {
    isOpen: false,
    className: '',
    items: [],
    changeBg: () => {},
    favList: () => {},
    openModal: () => {}
};