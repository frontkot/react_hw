import React, { useState, useEffect } from 'react';
import Header from './components/Header/Header'
import Footer from './components/Footer/Footer';
import Modal from './components/Modal/Modal'
import AppRoutes from './routes/AppRoutes'
import axios from 'axios';


import './Container.scss';
import './App.css';

const checkFav = () => {
  const arr = JSON.parse(localStorage.getItem('favorites-items'))
  if (arr !== null) {
      return arr
  } else {
    return []
  }
}

const checkCart = () => {
  const arr = JSON.parse(localStorage.getItem('cart-items'))

  if (arr !== null) {
      return arr
  } else {
    return []
  }
}


function App() {

  const [isOpen, setIsOpen] = useState(true);
  const [content, setContent] = useState([]);
  const [favList, setFavList] = useState(checkFav());
  const [openModal, setOpenModal] = useState(false);
  const [cartList, setCartList] = useState(checkCart());
  const [item, setItem] = useState('');
  
  const getContent = () => {
    return axios('./items.json')
    .then(res => setContent(res.data.items))
  }
  
  useEffect(() => {
    getContent()
  }, [])
 
  const changeFavorites = (id, key, item) => {
    let arr = item;
    let prod = content.filter(e => e.article.includes(id))[0]
    let articeArr = arr.map(e => e.article)

    if (articeArr.includes(id)) {
      arr = arr.filter(i => i.article !== id)
    } else {
      arr.push(prod);
    }
    localStorage.setItem(key, JSON.stringify(arr))

    if (key === 'favorites-items') {
      setFavList(JSON.parse(localStorage.getItem('favorites-items')))
    } else if (key === 'cart-items') {
      setCartList(arr)
    }

 
  }

  const addToCart = () => {
    const ls = JSON.parse(localStorage.getItem('cart-items'))
    if(ls !== null) {
      if (ls.map(e => e.article).includes(item)) {
        alert('This product already added to cart')
      } else {
        alert('You add this product to cart');
        changeFavorites(item, 'cart-items', cartList)
      }
    } else {
      alert('You add this product to cart');
      changeFavorites(item, 'cart-items', cartList)
    }
    setOpenModal(false);
  } 



  return (
    <div className='App'>
      <Header 
        className='main-header' 
        showContent={() => setIsOpen(!isOpen)}
        isOpen={isOpen}
        favList={favList}
        cartList={cartList}
      />
      <AppRoutes 
        className='main-content'
        isOpen={isOpen}
        content={content}
        changeBg={(e) => changeFavorites(e.target.id, 'favorites-items', favList)}
        favList={favList}
        cartList={cartList}
        openModal={(e) => {
          setOpenModal(!openModal)
          setItem(e.target.id)
        }}
      />
      {window.location.pathname  === '/cart' && 
      <Modal 
        header={'Remove product'} 
        onClick={() => setOpenModal(!openModal)}
        text={'Do you want remove product from Cart?'} 
        isOpen={openModal}
        onSubmit={(e) => {
          setOpenModal(!openModal)
          changeFavorites(item, 'cart-items', cartList)
        }}
      />
      }
      {window.location.pathname  !== '/cart' &&
      <Modal 
          header={'Choose this product'} 
          onClick={() => setOpenModal(!openModal)}
          text={'The item will be added to the cart'} 
          isOpen={openModal}
          onSubmit={(e) => addToCart(e)}
        />
    }
      <Footer 
        className='footer-bg'
        text='Aaa...Together because...'
      />     
    </div>
  );
}

export default App;


