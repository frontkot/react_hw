import React from 'react';
import PropTypes from 'prop-types';
import ListItem from '../ListItem/ListItem';

import './Content.scss'

const Content = ({
   header, isOpen, className, content, changeBg, favList, cartList, openModal, sortBy
}) => {


    return (
        <div className='container'>
            {isOpen && <div 
                    className={className}
                >
                    <h3>{header}</h3>
                    <ListItem 
                        className='list-item' 
                        items={content} 
                        changeBg={changeBg}
                        favList={favList}
                        cartList={cartList}
                        sortBy={sortBy}
                        openModal={openModal}
                        />
                </div>
            }
        </div>
    );
};

export default Content;

Content.propTypes = {
    isOpen: PropTypes.bool,
    content: PropTypes.array,
    favList: PropTypes.array,
    changeBg: PropTypes.func,
    openModal: PropTypes.func,
    className: PropTypes.string
};

Content.defaultProps = {
    isOpen: false,
    className: '',
    content: [],
    favList: [],
    changeBg: () => {},
    openModal: () => {}
};