import React from 'react';
import Button from '../Button/Button';
import {NavLink} from "react-router-dom";

import './Header.scss'

const Header = ({
    className, showContent, isOpen, favList, cartList
}) => {
    const favNum = favList.length;
    const cartNum = cartList.length;

    return (
        <div className='header-bg'>
            <div className='container'>
                <div className={className}>
                    <NavLink exact to='/' activeStyle={{color: 'coral'}} className='nav-link'>
                        Home
                    </NavLink>
                    <NavLink exact to='/favorites' activeStyle={{color: 'coral'}} className='nav-link'>
                        Favorites
                        {favNum > 0 && <span className='count-num'>{favNum}</span>}
                    </NavLink>
                    <NavLink exact to='/cart' activeStyle={{color: 'coral'}} className='nav-link'>
                        Cart
                        {cartNum > 0 && <span className='count-num'>{cartNum}</span>}
                    </NavLink>
                    <Button 
                        className='header-button' 
                        onClick={showContent}
                    >
                        {isOpen ? 'Hide content'  : 'Show content'}
                    </Button>
                </div>
            </div>
        </div>
    );
};

export default Header;

Header.defaultProps = {
    className: 'header',
    showContent: false
}