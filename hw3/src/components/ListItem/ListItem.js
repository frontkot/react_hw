import React from 'react';
import Item from '../Item/Item'
import PropTypes from 'prop-types';

import './ListItem.scss'

const ListItem = ({
    items, className, changeBg, favList, cartList, openModal, sortBy
}) => {
    
    let product;
    if (sortBy === 'favorites') {
        product = favList
    } else if (sortBy === 'cart'){
        product = cartList
    } else {
        product = items
    }
      
    const productItems = ( 
        product.map((e) => 
            <Item 
                className='product-item' 
                changeBg={changeBg}
                name={e.name}
                price={e.price}
                url={e.url}
                article={e.article}
                color={e.color}
                key={e.article}
                favItem={favList.map(e => e.article).includes(e.article) ? true : false}
                openModal={openModal}
                cartList={sortBy === 'cart' ? true : false}
            />
    )
)
    const length = productItems.length > 0 ? true : false
    
    return (
        length ? <ul className={className}>{productItems}</ul> : <h3>You don't selected any product yet :'(</h3>
    );
};

export default ListItem;


ListItem.propTypes = {
    isOpen: PropTypes.bool,
    className: PropTypes.string,
    items: PropTypes.array,
    changeBg: PropTypes.func,
    favList: PropTypes.array,
    openModal: PropTypes.func
};

ListItem.defaultProps = {
    isOpen: false,
    className: '',
    items: [],
    changeBg: () => {},
    favList: () => {},
    openModal: () => {}
};