import React from 'react';
import {Route, Switch} from "react-router-dom";
import Page404 from "../pages/Page404/Page404";
import Content from '../components/Content/Content';
import PropTypes from 'prop-types';


const AppRoutes = ({
    isOpen, className, content, changeBg, favList, cartList, openModal 
}) => {

    return (
        <>
            <Switch>
                <Route 
                    exact 
                    path='/' 
                    render={() => <Content
                        header='Our products'
                        className={className}
                        isOpen={isOpen}
                        content={content}
                        changeBg={changeBg}
                        favList={favList}
                        openModal={openModal}
                    />}
                />
                <Route
                    path='/favorites'
                    exact
                    render={() => <Content
                        header='Your favorite products'
                        className={className}
                        isOpen={isOpen}
                        content={content}
                        changeBg={changeBg}
                        favList={favList}
                        openModal={openModal}
                        sortBy={'favorites'}
                    />}

                />
                <Route 
                    exact 
                    path='/cart' 
                    render={() => <Content
                        header='Products in Cart'
                        className={className}
                        isOpen={isOpen}
                        content={content}
                        changeBg={changeBg}
                        favList={favList}
                        cartList={cartList}
                        openModal={openModal}
                        sortBy={'cart'}
                    />}

                />
                <Route 
                    path='*' 
                    component={(routeProps) => <Page404 {...routeProps}/>} 
                />
            </Switch>
        </>
    );
};

export default AppRoutes;

AppRoutes.propTypes = {
    isOpen: PropTypes.bool,
    content: PropTypes.array,
    favList: PropTypes.array,
    cartList: PropTypes.array,
    changeBg: PropTypes.func,
    openModal: PropTypes.func,
    className: PropTypes.string
  };
  
  AppRoutes.defaultProps = {
    isOpen: false,
    className: '',
    content: [],
    favList: [],
    cartList: [],
    changeBg: () => {},
    openModal: () => {}
  };